<?php 
if(isset($_POST['action']) && !empty($_POST['action'])) {
    if($_POST['action'] == 'post_contact_form'){
        $first = $_POST['first'];
        $last = $_POST['last'];
        $email = $_POST['email'];
        $destination = $_POST['destination'];
        $message = $_POST['message'];
        
        $results = array();
        $errors = array();
        if(empty($first)){
            array_push($errors,'please enter your first name');
        }
        if(empty($last)){
            array_push($errors,'please enter your last name');
        }
        if(empty($email)){
            array_push($errors,'please enter your email address');
        }
        else if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
            array_push($errors,'please enter a valid email address');
        }
        if(empty($message)){
            array_push($errors,'please enter your message');
        }
if(count($errors) > 0){
            $results['status'] = "error";
            $results['errors'] = $errors;
            echo json_encode($results);
}
else{
    sendFormDetailsMail($first,$last,$email,$destination,$message);
    $results['status'] = "success";
    echo json_encode($results);
}

        die();
    }
}

function sendFormDetailsMail($first,$last,$email,$destination,$message){
    $subject = 'New message from Tribal Quest ';

    $body = "<div style='text-align: left; direction: ltr;'>";
    $body .= "Name: ".$first." ".$last."<br/><br/>";
    $body .= "Email: ".$email."<br/><br/>";
    $body .= "destination: ".$destination."<br/><br/>";
    $body .= "message: ".$message;
    $body .= "</div>";
    $headers = 'Content-Type: text/html; charset=UTF-8';

    mail( 'team.namibia@myheritage.com', $subject, $body, $headers );
}
?>
