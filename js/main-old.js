// Global vars
    var mobileRes = 320,
    tabletRes = 768,
    desktopRes = 1024,
    // desktopHdRes = 1280,
    appResizeTimer,
    smController;

$(document).ready(function() {
    // On ready functions run
        videoBackground();
        fullscreen();
        scrollbars();

        //set form functions
        setFormMethods();

    // On window load
        $(window).load(function() {
            $('body').addClass('loaded');
            scrollingAnimation();
        });

    // On scroll run
        $(window).scroll(function() {

        });

    // On window resize
        $(window).resize(function() {
            clearTimeout(appResizeTimer);

            appResizeTimer = setTimeout(function(){
                videoBackground();
                fullscreen();
                scrollbars();
            }, 50);
        });

    // On click outside all widgets
        $(document).on('click tap', function(e) {
            var $this = $(e.target);
            if(!$this.closest('.open_lightbox').length && !$this.closest('.lightbox .box .inner').length)
            {
                var $lightboxCont = $('#lightbox');
                if($lightboxCont.hasClass('not_active'))
                {
                    $lightboxCont.removeClass('active').velocity('fadeOut',{duration:200,easing:'ease'});
                    TweenMax.to($('#lightbox .box:visible'), 0.2,{scale:0.9, ease: Power1.easeOut, force3D: !0,onComplete:function(){
                        $('#lightbox .box').hide();
                    }});
                }
            } 
        });

    // Fullscreen
        function fullscreen(){
            if($('.fullscreen').length>0)
            {
                if(mobileRes < getViewport_width())
                {
                    $('.fullscreen').height(getViewport_height());
                }
                else
                {
                    var innerHeight = $('.fullscreen').innerHeight();
                    innerHeight = innerHeight > getViewport_height() ? innerHeight : getViewport_height();                                         
                    $('.fullscreen').css({'height':innerHeight });
                }
            }

             // scale coming soon 

            $( "a li.papua" ).click(function() {
              $('#soon').css('font-size', '24px'); },400,
              function () {
              $('#soon').css('font-size', '12px');
            });

        }


    // Scroll to
        $('a[href*=#]:not([href=#])').on('click',function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname)
            {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('body').addClass('scrolling');
                    target.velocity('scroll',{ duration:750,easing: 'easeInOutQuart',offset:-$('#header').height(),complete:function(){
                        $('body').removeClass('scrolling');
                    }});
                    return false;
                }
            }
        });

    // Lightbox
        $('.open_lightbox').on('click',function(){
            var $lightboxCont = $('#lightbox'),
            $thisLightbox = '.'+$(this).attr('data-lightbox'),
            $thisVideo = $(this).attr('data-video'),
            $thisDataSlide = $(this).data('slide')-1;
            if(!$lightboxCont.hasClass('active'))
            {
                $('#lightbox .box').hide();
                $($thisLightbox).show();
                $lightboxCont.addClass('active').velocity('fadeIn',{duration:300,easing:'ease'});
                TweenMax.fromTo('#lightbox '+$thisLightbox, 0.3,{scale:0.9},{scale:1, force3D: !0, ease: Power1.easeOut});
            }
            return false;
        });
        
        $('#lightbox').on('click','.close',function(){
            $lightboxCont = $('#lightbox');
            $lightboxCont.removeClass('active').velocity('fadeOut',{duration:200,easing:'ease'});
            TweenMax.to($('#lightbox .box:visible'), 0.2,{scale:0.9, ease: Power1.easeOut, force3D: !0,onComplete:function(){
                $('#lightbox .box').hide();
            }});
            return false;
        });
});

// Functions
    // Recognize ie
        function isIE()
        {
            var myNav = navigator.userAgent.toLowerCase();
            return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
        }

    // IE9 and older placeholder support
        !function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){function b(b){var c={},d=/^jQuery\d+$/;return a.each(b.attributes,function(a,b){b.specified&&!d.test(b.name)&&(c[b.name]=b.value)}),c}function c(b,c){var d=this,f=a(d);if(d.value==f.attr("placeholder")&&f.hasClass(m.customClass))if(f.data("placeholder-password")){if(f=f.hide().nextAll('input[type="password"]:first').show().attr("id",f.removeAttr("id").data("placeholder-id")),b===!0)return f[0].value=c;f.focus()}else d.value="",f.removeClass(m.customClass),d==e()&&d.select()}function d(){var d,e=this,f=a(e),g=this.id;if(""===e.value){if("password"===e.type){if(!f.data("placeholder-textinput")){try{d=f.clone().attr({type:"text"})}catch(h){d=a("<input>").attr(a.extend(b(this),{type:"text"}))}d.removeAttr("name").data({"placeholder-password":f,"placeholder-id":g}).bind("focus.placeholder",c),f.data({"placeholder-textinput":d,"placeholder-id":g}).before(d)}f=f.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id",g).show()}f.addClass(m.customClass),f[0].value=f.attr("placeholder")}else f.removeClass(m.customClass)}function e(){try{return document.activeElement}catch(a){}}var f,g,h="[object OperaMini]"==Object.prototype.toString.call(window.operamini),i="placeholder"in document.createElement("input")&&!h,j="placeholder"in document.createElement("textarea")&&!h,k=a.valHooks,l=a.propHooks;if(i&&j)g=a.fn.placeholder=function(){return this},g.input=g.textarea=!0;else{var m={};g=a.fn.placeholder=function(b){var e={customClass:"placeholder"};m=a.extend({},e,b);var f=this;return f.filter((i?"textarea":":input")+"[placeholder]").not("."+m.customClass).bind({"focus.placeholder":c,"blur.placeholder":d}).data("placeholder-enabled",!0).trigger("blur.placeholder"),f},g.input=i,g.textarea=j,f={get:function(b){var c=a(b),d=c.data("placeholder-password");return d?d[0].value:c.data("placeholder-enabled")&&c.hasClass(m.customClass)?"":b.value},set:function(b,f){var g=a(b),h=g.data("placeholder-password");return h?h[0].value=f:g.data("placeholder-enabled")?(""===f?(b.value=f,b!=e()&&d.call(b)):g.hasClass(m.customClass)?c.call(b,!0,f)||(b.value=f):b.value=f,g):b.value=f}},i||(k.input=f,l.value=f),j||(k.textarea=f,l.value=f),a(function(){a(document).delegate("form","submit.placeholder",function(){var b=a("."+m.customClass,this).each(c);setTimeout(function(){b.each(d)},10)})}),a(window).bind("beforeunload.placeholder",function(){a("."+m.customClass).each(function(){this.value=""})})}});
        $('input, textarea').placeholder();

    // Get viewport width and height
        function getViewport_width()
        {
        	var viewPortWidth;
        	if(typeof window.innerWidth != 'undefined') {viewPortWidth = window.innerWidth}
        	else if(typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0){viewPortWidth = document.documentElement.clientWidth}
        	else{viewPortWidth = document.getElementsByTagName('body')[0].clientWidth;}
        	return viewPortWidth;
        }

        function getViewport_height()
        {
        	var viewPortHeight;
        	if(typeof window.innerHeight != 'undefined') {viewPortHeight = window.innerHeight}
        	else if(typeof document.documentElement != 'undefined' && typeof document.documentElement.clientHeight != 'undefined' && document.documentElement.clientHeight != 0){viewPortHeight = document.documentElement.clientHeight}
        	else{viewPortHeight = document.getElementsByTagName('body')[0].clientHeight;}
        	return viewPortHeight;
        }

     // Video bcakground
        function videoBackground()
        {
            $('.mainvideo').each(function(){
                var videoUrl = $(this).data('video-url');
                if(tabletRes < getViewport_width())
                {
                    if(!$(this).hasClass('videobg'))
                    {
                        if($(this).find('.video_pic').length > 0)
                            $(this).find('.video_pic').remove();
                        $(this).addClass('videobg');
                        $(this).vide({
                            'mp4': videoUrl,
                            'webm': videoUrl,
                            'ogv': videoUrl,
                            'poster': videoUrl,

                            'volume': '1',
                            'playbackRate': '1',
                            'muted': 'true',
                            'loop': '1',
                            'preload': 'none',
                            'autoplay': 'false',
                            'position': "50% 50%",
                            'posterType': "detect",
                            'resizing': 'true'
                        });
                    }
                }
                else
                {
                    if($(this).hasClass('videobg'))
                    {
                        $(this).removeClass('videobg');
                        $(this).data("vide").destroy();
                    }
                    if($(this).find('.video_pic').length == 0)
                        $(this).append('<div style="background-image:url(\''+videoUrl+'.jpg\');" class="video_pic cover"></div>');
                }
            });
        }

// toggle menu button

        var hamburger = $('#menu-icon');
          hamburger.click(function() 
        {
            hamburger.toggleClass('active');
            return false;
        });

        $("#menu-icon,#close-menu").click(function() 
        {
          $("#top-menu").slideToggle();
        });
 
// owl carousel setup

  $("#gallery_carousel").owlCarousel(
  {
 
      slideSpeed : 300,
      singleItem: true,
      paginationSpeed : 400,
      animateOut: 'fadeOut',
      items : 1, 
      itemsDesktop : false,
      itemsDesktopSmall : false,
      itemsTablet: false,
      itemsMobile : false,
      navigation: true,
      navigationText: [
      "<i class='icon-chevron-left icon-white'>test<</i>",
      "<i class='icon-chevron-right icon-white'>>test2</i>"
      ],
    });
 

  // Scrolling animation
        function scrollingAnimation()
        {
            smController = new ScrollMagic.Controller();


            // Homepage
            if($('.home').length > 0)
            {
                $('.home').removeClass('blur');
                TweenMax.fromTo('.home h1', 1.2, { letterSpacing:'15px',opacity:0 }, {letterSpacing:'0px',opacity:1,autoRound:false,delay:0.5, force3D: !0, ease:Power1.easeOut});
                TweenMax.staggerFromTo('.home .animate', 0.8, {opacity:0 }, {opacity:1,delay:1, force3D: !0, ease:Power1.easeOut},0.4);
            }
            

            // Himba
            if($('#himba').length > 0 && desktopRes <= getViewport_width()) 
            {
                TweenMax.fromTo('#himba .text-box h1', 1.2, { letterSpacing:'15px',opacity:0 }, {letterSpacing:'0px',opacity:1,autoRound:false,delay:0.5, force3D: !0, ease:Power1.easeOut});
                TweenMax.fromTo('#himba .text-box h2', 1.2, { letterSpacing:'15px',opacity:0 }, {letterSpacing:$('#himba .title_second').css('letter-spacing'),opacity:1,autoRound:false,delay:1, force3D: !0, ease:Power1.easeOut});
                TweenMax.fromTo('#himba .text', 0.7, {opacity:0,x:-30 }, {opacity:1,x:0,delay:1.5, force3D: !0, ease:Power1.easeOut});
                
                // Top parallax
                    new ScrollMagic.Scene({triggerElement: '#himba .top .meet .title',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#himba .top .box .text',{y:'-120%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);
                        
                    new ScrollMagic.Scene({triggerElement: '#himba .top .meet .title',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#himba .top .box .video',{y:'-60%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);
                        
                // Animate meet
                    var tween2 = TweenMax.staggerFromTo('#himba .families-icons .item',0.5,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut},0.15);
                    new ScrollMagic.Scene({triggerElement: 'himba .families-icons',triggerHook: "onEnter",offset:$('#himba .families-icons').height()/1.05})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                        
                // Bottom parallax
                    var tween2 = TweenMax.fromTo('#himba .bottom',2,{y:'-20%'},{y:'0%', force3D: !0, ease: Linear.easeNone});        
                    new ScrollMagic.Scene({triggerElement: '#himba .top .meet .btn',duration:"60%",triggerHook: "onEnter"})
                        .setTween(tween2)
                        .addTo(smController);
                        
                        
                // Animate bottom
                    var tween2 = TweenMax.staggerFromTo('#himba .numbers-icons .item',0.5,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut},0.15);
                    new ScrollMagic.Scene({triggerElement: '#himba .numbers-icons',triggerHook: "onEnter",offset:$('#himba .numbers-icons').height()/1.05})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
            }

            // Family animation
            if($('body').hasClass('hembinda') && desktopRes <= getViewport_width()) 
            {
                // Section 1
                if($('#hero').length > 0)
                {
                    TweenMax.fromTo('#hero', 3.2, { scale:1.1 }, { scale:1, force3D: !0, ease:Power1.easeOut});
                    TweenMax.fromTo('#hero h1,#hero .table', 1.6, { opacity:0,scale:0.9 }, { opacity:1,scale:1,delay:0.5, force3D: !0, ease:Power1.easeOut});

                    new ScrollMagic.Scene({triggerElement: '#hero',duration:"100%",triggerHook: "onLeave"})
                        .setTween('#hero h1',{letterSpacing:15,autoRound:false, force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);
                }

                // Section 2
                if($('#about').length > 0)
                {

                    var tween2 = TweenMax.staggerFromTo('#about .image_big .img',2.5,{opacity:0,x:250},{opacity:1,x:0, force3D: !0, ease: Power2.easeOut},0.2);
                    new ScrollMagic.Scene({triggerElement: '#about'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#about .inner-page .animate',1,{opacity:0,y:60},{opacity:1,y:0,delay:0.8, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#about .big-text-box',triggerHook: "onEnter",offset:$('#about .big-text-box').height()/2})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#about .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#about .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    //function that draws the svg using lineLength
                    function pathPrepare ($el) {
                            var lineLength = $el[0].getTotalLength();
                            $el.css("stroke-dasharray", lineLength);
                            $el.css("stroke-dashoffset", lineLength);
                        }

                    function lineWidth ($el) {
                            var line_Width = $el2[0].width();
                            $el.css("stroke-dasharray", lineLength);
                            $el.css("stroke-dashoffset", lineLength);
                        }

                    //Green Sock Variables
                    var $line1 = $("#Layer0_0_1_STROKES");

                    //prepare SVG path
                    pathPrepare($line1);

                    var controller = new ScrollMagic.Controller();
                    var tween = new TimelineMax()
                         .add(TweenMax.to($line1 , 2 , {strokeDashoffset: 0, ease:Linear.easeNone}))

                    var scene = new ScrollMagic.Scene({triggerElement: "#about .tree .three", triggerHook: 'onEnter', duration: 1200, tweenChanges: true,})
                    .reverse(false)
                    .addTo(controller)
                    .setTween(tween);

                    var tween2 = TweenMax.staggerFromTo('.tree .item',0.4,{opacity:0},{opacity:1, force3D: !0, ease: Power2.easeOut},0.1);
                    new ScrollMagic.Scene({triggerElement: "#about .tree .three", triggerHook: 'onEnter', duration: 600, tweenChanges: true,})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

                // Section 3
                if($('#name').length > 0)
                {
                    var tween2 = TweenMax.staggerFromTo('#name .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0,delay:0.8, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#name .big-text-box',triggerHook: "onEnter",offset:$('#name .big-text-box').height()/1.5})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#name',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#name .big-text-box',{y:'-80%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#name .child',2,{opacity:0,y:150},{opacity:1,y:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#name .big-text-box',triggerHook: "onEnter",offset:$('#name .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

                // Strip 1
                if($('#member1').length > 0)
                {
                    new ScrollMagic.Scene({triggerElement: '#member1',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#member1 video',{y:'50%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var orig = document.querySelector('#member1 .circle');
                    var obj = {length:0, pathLength:400};

                    var tween =  new TimelineMax().add([
                        TweenLite.to("#member1 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member1 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member1 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj, 1.5, {length:obj.pathLength, onUpdate:function(){orig.style.strokeDasharray = [obj.length,obj.pathLength].join(' ');}, ease:Linear.easeNone})
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member1',triggerHook: "onEnter",offset:$('#member1').height()/1.5})
                        .setTween(tween)
                        .reverse(false)
                        .addTo(smController);

                }

                // Strip 2
                if($('#member2').length > 0)
                {
                    var orig2 = document.querySelector('#member2 .circle');
                    var obj2 = {length:0, pathLength:400};

                    var tween2 =  new TimelineMax().add([
                        TweenLite.to("#member2 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member2 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member2 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj2, 1.5, {length:obj2.pathLength, onUpdate:function(){orig2.style.strokeDasharray = [obj2.length,obj2.pathLength].join(' ');}, ease:Linear.easeNone}),
                        TweenMax.staggerFromTo('#member2 .image_big',1.8,{opacity:0,x:-150},{opacity:1,x:0,delay:0.3, force3D: !0, ease: Power2.easeOut},0.5)
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member2',triggerHook: "onEnter",offset:$('#member2').height()/1.5})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                }

                // Strip 3
                if($('#member3').length > 0)
                {
                    new ScrollMagic.Scene({triggerElement: '#member3',duration:"200%",triggerHook: "onEnter"}) 
                        .setTween('#member3 video',{y:'50%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var orig3 = document.querySelector('#member3 .circle');
                    var obj3 = {length:0, pathLength:400};

                    var tween =  new TimelineMax().add([
                        TweenLite.to("#member3 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member3 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member3 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj3, 1.5, {length:obj3.pathLength, onUpdate:function(){orig3.style.strokeDasharray = [obj3.length,obj3.pathLength].join(' ');}, ease:Linear.easeNone})
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member3',triggerHook: "onEnter",offset:$('#member3').height()/1.5})
                        .setTween(tween)
                        .reverse(false)
                        .addTo(smController);

                }

                // Section 4
                if($('#drought').length > 0)
                {
                    // First

                    var tween2 = TweenMax.staggerFromTo('#drought .first .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#drought .first',triggerHook: "onEnter",offset:$('#drought').height()/2.6})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#drought .first .big-text-box h3 span',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#drought .first',triggerHook: "onEnter",offset:$('#drought').height()/3.0})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#drought .first .big-text-box',duration:"200%",offset:$('#drought .first').height()/1.2,triggerHook: "onEnter"})
                        .setTween('#drought .first .big-text-box',{y:'-80%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    // Second

                    var tween2 = TweenMax.staggerFromTo('#drought .second .big-text-box .animate',0.8,{opacity:0,y:70},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#drought .second',triggerHook: "onEnter",offset:$('#drought').height()/3.4})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#drought .second .big-text-box',duration:"200%",offset:$('#drought .second').height()/1.8,triggerHook: "onEnter"})
                        .setTween('#drought .second .big-text-box',{y:'-80%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                }

                // Section 5
                if($('#wives').length > 0)
                {

                    // Mud
                    var tween5 =  new TimelineMax().add([
                        TweenMax.fromTo('#wives .backgrounds .one .item_1',1.8,{x:'-5%',y:'-40%'},{x:'0%',y:'0%', force3D: !0, ease: Linear.easeNone}),
                        TweenMax.fromTo('#wives .backgrounds .one .item_2',1.8,{x:'150%',y:'-50%',rotation:15},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Linear.easeNone}),
                        TweenMax.fromTo('#wives .backgrounds .one .item_3',1.8,{x:'60%',y:'-55%',rotation:20},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Linear.easeNone}),
                        TweenMax.fromTo('#wives .backgrounds .two .item_1',1.8,{x:'40%',y:'-55%',rotation:-7},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Linear.easeNone}),
                        TweenMax.fromTo('#wives .backgrounds .two .item_2',1.8,{x:'38%',y:'-84%',rotation:-2},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Linear.easeNone}),

                    ]);

                    new ScrollMagic.Scene({triggerElement: '#wives .backgrounds',triggerHook: "onEnter",duration:'80%',offset:$('#wives .backgrounds').height()*0.2})
                        .setTween(tween5)
                        .addTo(smController);


                    var tween2 = TweenMax.staggerFromTo('#wives .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#wives .big-text-box',triggerHook: "onEnter",offset:$('#wives .big-text-box').height()/1.75})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#wives .big-text-box h3 span',1,{opacity:0,y:40},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#wives .big-text-box',triggerHook: "onEnter",offset:$('#wives .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#wives .innerpage',duration:"200%",triggerHook: "onEnter",offset:$('#wives .innerpage').height()/1.8})
                        .setTween('#wives .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.fromTo('#wives .image_big',2.3,{x:'50%'},{x:'0%', force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#wives',triggerHook: "onEnter",duration:'200%'})
                        .setTween(tween2)
                        .addTo(smController);
                }

                // Section 6
                if($('#steps').length > 0)
                {
                    // title section
                    var tween2 = TweenMax.fromTo('#steps .title-cloud-dark',0.8,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut});
                    new ScrollMagic.Scene({triggerElement: '#steps',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.fromTo('#steps .steps_desc',0.8,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut});
                    new ScrollMagic.Scene({triggerElement: '#steps .steps_desc'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    // prayers
                    $('#steps .steps-inner .item').each(function(index,value){
                        var $thisNum = index+1;
                        var $this = $(this);
                        var $thisSeprator = $('.steps-inner .sep_'+$thisNum);
                        var axisX = $this.hasClass('item_right') ? -60 : 60;
                        var offsetY = 3 ? 1.1 : 1.5;

                        var tween =  TweenMax.fromTo($this.find('.animate'), 0.8,{x:-100,opacity:0},{x: 0,opacity:1, force3D: !0, ease: Power2.easeOut});

                        var scene56777 = new ScrollMagic.Scene({triggerElement: '#steps .steps-inner .item_'+$thisNum, offset: $('#steps .steps-inner .item'+$thisNum).height()*offsetY})
                            .setTween(tween)
                            .addTo(smController);

                        // Seprator
                        var tween =  TweenMax.fromTo($this.find('.arrow span'), 0.8,{width: '0%'},{width: '100%', force3D: !0, ease: Linear.easeNone});

                        var scene3333 = new ScrollMagic.Scene({triggerElement: '#steps .steps-inner .item_'+$thisNum, offset: 100,duration:$this.find('.arrow').height()*2})
                            .setTween(tween)
                            .addTo(smController);
                    });
                }

                // Section 7
                if($('#life').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#life .title-cloud-dark',0.8,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut});
                    new ScrollMagic.Scene({triggerElement: '#life',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

            }

            // Family F_2 animation

            if($('body').hasClass('tjisemo') && desktopRes <= getViewport_width()) 
            {
                // Section 1
                if($('#hero').length > 0)
                {
                    TweenMax.fromTo('#hero', 3.2, { scale:1.1 }, { scale:1, force3D: !0, ease:Power1.easeOut});
                    TweenMax.fromTo('#hero h1,#hero .table', 1.6, { opacity:0,scale:0.9 }, { opacity:1,scale:1,delay:0.5, force3D: !0, ease:Power1.easeOut});

                    new ScrollMagic.Scene({triggerElement: '#hero',duration:"100%",triggerHook: "onLeave"})
                        .setTween('#hero h1',{letterSpacing:15,autoRound:false, force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);
                }

                // Section 2
                if($('#about').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#about .title-cloud',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#about .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = 
                    TweenMax.staggerFromTo('#about .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#about .big-text-box',triggerHook: "onEnter",offset:$('#about .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#about .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#about .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#about .image_big .img',2.3,{opacity:0,x:150},{opacity:1,x:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#about .big-text-box',triggerHook: "onEnter",offset:$('#about .big-text-box').height()/5})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                //function that draws the svg using lineLength
                    function pathPrepare ($el) {
                            var lineLength = $el[0].getTotalLength();
                            $el.css("stroke-dasharray", lineLength);
                            $el.css("stroke-dashoffset", lineLength);
                        }

                    function lineWidth ($el) {
                            var line_Width = $el2[0].width();
                            $el.css("stroke-dasharray", lineLength);
                            $el.css("stroke-dashoffset", lineLength);
                        }

                    //Green Sock Variables
                    var $line1 = $("#Layer0_0_1_STROKES");
                    var $items = $(".tree .item");

                    //prepare SVG path
                    pathPrepare($line1);


                    var controller = new ScrollMagic.Controller();
                    var tween = new TimelineMax()
                         .add(TweenMax.to($line1 , 2 , {strokeDashoffset: 0, ease:Linear.easeNone}))

                    //Create ScrollMagic Scene
                    var scene = new ScrollMagic.Scene({triggerElement: "#about .tree .three", triggerHook: 'onEnter', duration: 1200, tweenChanges: true,})
                    .reverse(false)
                    .addTo(controller)
                    .setTween(tween);

                    var tween2 = TweenMax.staggerFromTo('.tree .item',0.4,{opacity:0},{opacity:1, force3D: !0, ease: Power2.easeOut},0.1);
                    new ScrollMagic.Scene({triggerElement: "#about .tree .three", triggerHook: 'onEnter', duration: 600, tweenChanges: true,})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                }

                if($('#name').length > 0)
                {

                    var tween2 = TweenMax.staggerFromTo('#name .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#name .big-text-box',triggerHook: "onEnter",offset:$('#name .big-text-box').height()/1.5})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#name',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#name .big-text-box',{y:'-80%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#name .child',2,{opacity:0,y:250},{opacity:1,y:0,delay:1.2, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#name .big-text-box',triggerHook: "onEnter",offset:$('#name .big-text-box').height()/1.15})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

                                // Strip 1
                if($('#member1').length > 0)
                {
                    new ScrollMagic.Scene({triggerElement: '#member1',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#member1 video',{y:'50%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var orig = document.querySelector('#member1 .circle');
                    var obj = {length:0, pathLength:400};

                    var tween =  new TimelineMax().add([
                        TweenLite.to("#member1 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member1 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member1 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj, 1.5, {length:obj.pathLength, onUpdate:function(){orig.style.strokeDasharray = [obj.length,obj.pathLength].join(' ');}, ease:Linear.easeNone})
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member1',triggerHook: "onEnter",offset:$('#member1').height()/1.5})
                        .setTween(tween)
                        .reverse(false)
                        .addTo(smController);

                }

                // Strip 2
                if($('#member2').length > 0)
                {
                    var orig2 = document.querySelector('#member2 .circle');
                    var obj2 = {length:0, pathLength:400};

                    var tween2 =  new TimelineMax().add([
                        TweenLite.to("#member2 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member2 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member2 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj2, 1.5, {length:obj2.pathLength, onUpdate:function(){orig2.style.strokeDasharray = [obj2.length,obj2.pathLength].join(' ');}, ease:Linear.easeNone}),
                        TweenMax.staggerFromTo('#member2 img',1.8,{opacity:0,x:1000},{opacity:1,x:0,delay:0.3, force3D: !0, ease: Power2.easeOut},0.5)
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member2',triggerHook: "onEnter",offset:$('#member2').height()/1.5})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                }

                // Strip 3
                if($('#member3').length > 0)
                {
                    new ScrollMagic.Scene({triggerElement: '#member3',duration:"200%",triggerHook: "onEnter"}) 
                        .setTween('#member3 video',{y:'50%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var orig3 = document.querySelector('#member3 .circle');
                    var obj3 = {length:0, pathLength:400};

                    var tween =  new TimelineMax().add([
                        TweenLite.to("#member3 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member3 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member3 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj3, 1.5, {length:obj3.pathLength, onUpdate:function(){orig3.style.strokeDasharray = [obj3.length,obj3.pathLength].join(' ');}, ease:Linear.easeNone}),
                        TweenMax.staggerFromTo('#member3 img',2.8,{opacity:0,y:400},{opacity:1,y:0,delay:0.3, force3D: !0, ease: Power2.easeOut},0.5)
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member3',triggerHook: "onEnter",offset:$('#member3').height()/1.5})
                        .setTween(tween)
                        .reverse(false)
                        .addTo(smController);

                }

                // Section 4
                if($('#coming-of-age').length > 0)
                {
                    
                    var tween2 = TweenMax.staggerFromTo('#coming-of-age #pipe-man img',4,{opacity:0,y:250},{opacity:1,y:0,delay:1.2, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#coming-of-age #pipe-man img',triggerHook: "onEnter"})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.fromTo('#coming-of-age .title-cloud',1,{opacity:0,y:70},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#coming-of-age .first',triggerHook: "onEnter",offset:$('#coming-of-age').height()/3.0})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#coming-of-age .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#coming-of-age',triggerHook: "onEnter",offset:$('#coming-of-age').height()/3.0})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#coming-of-age',duration:"200%",offset:$('#coming-of-age .first').height()/1.2,triggerHook: "onEnter"})
                        .setTween('#coming-of-age .big-text-box',{y:'-80%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween5 =  new TimelineMax().add([
                        TweenMax.from('#coming-of-age .img_1',0.7,{x:'600px',rotation:360},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_2',0.7,{x:'600px',rotation:360},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_3',0.7,{x:'600px',rotation:300},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_4',0.7,{x:'600px',rotation:360},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_5',0.7,{x:'600px',rotation:80},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_6',0.7,{x:'600px',rotation:80},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_7',0.7,{x:'600px',rotation:80},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_8',0.7,{x:'600px',rotation:80},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},0.4),
                        TweenMax.from('#coming-of-age .img_9',0.7,{x:'600px',rotation:80},{x:'0%',y:'0%',rotation:0, force3D: !0, ease: Power1.easeOut},2),

                    ]);

                    new ScrollMagic.Scene({triggerElement: '#coming-of-age .image_big',triggerHook: "onEnter",duration:'30%',offset:$('#coming-of-age .image_big').height()*0.4})
                        .setTween(tween5)
                        .addTo(smController);
                }

                // Section 5
                if($('#family-feud').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#family-feud .title-cloud',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#family-feud .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#family-feud .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#family-feud .big-text-box',triggerHook: "onEnter",offset:$('#family-feud .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#family-feud .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#family-feud .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#family-feud img.image_big ',2.3,{opacity:0,y:150},{opacity:1,y:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#family-feud .big-text-box',triggerHook: "onEnter",offset:$('#family-feud .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

                // Section 6
                if($('#marriage').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#marriage .title-cloud-dark',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#marriage .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#marriage .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#marriage .big-text-box',triggerHook: "onEnter",offset:$('#marriage .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#marriage .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#marriage .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#marriage img.image_big ',2.3,{opacity:0,y:150},{opacity:1,y:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#marriage .big-text-box',triggerHook: "onEnter",offset:$('#marriage .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

                 // Section 7
                if($('#father-died').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#father-died .title-cloud',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#father-died .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#father-died .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#father-died .big-text-box',triggerHook: "onEnter",offset:$('#father-died .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#father-died .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#father-died .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController); 
                }

                  // Section 8
                if($('#life').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#life .title-cloud-dark',0.8,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#life',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

            }

             // Family F_3 animation

            if($('body').hasClass('mbinge') && desktopRes <= getViewport_width()) 
            {
                // Section 1
                if($('#hero').length > 0)
                {
                    TweenMax.fromTo('#hero', 3.2, { scale:1.1 }, { scale:1, force3D: !0, ease:Power1.easeOut});
                    TweenMax.fromTo('#hero h1,#hero .table', 1.6, { opacity:0,scale:0.9 }, { opacity:1,scale:1,delay:0.5, force3D: !0, ease:Power1.easeOut});

                    new ScrollMagic.Scene({triggerElement: '#hero',duration:"100%",triggerHook: "onLeave"})
                        .setTween('#hero h1',{letterSpacing:15,autoRound:false, force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);
                }

                // Section 2
                if($('#about').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#about .title-cloud',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#about .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#about .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#about .big-text-box',triggerHook: "onEnter",offset:$('#about .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#about .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#about .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#about .image_big .img',2.3,{opacity:0,x:150},{opacity:1,x:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#about .big-text-box',triggerHook: "onEnter",offset:$('#about .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                     //function that draws the svg using lineLength
                    function pathPrepare ($el) {
                            var lineLength = $el[0].getTotalLength();
                            $el.css("stroke-dasharray", lineLength);
                            $el.css("stroke-dashoffset", lineLength);
                        }

                    function lineWidth ($el) {
                            var line_Width = $el2[0].width();
                            $el.css("stroke-dasharray", lineLength);
                            $el.css("stroke-dashoffset", lineLength);
                        }

                    //Green Sock Variables
                    var $line1 = $("#Layer0_0_1_STROKES");

                    //prepare SVG path
                    pathPrepare($line1);

                    var controller = new ScrollMagic.Controller();
                    var tween = new TimelineMax()
                         .add(TweenMax.to($line1 , 2 , {strokeDashoffset: 0, ease:Linear.easeNone}))

                    //Create ScrollMagic Scene
                    var scene = new ScrollMagic.Scene({triggerElement: "#about .tree .three", triggerHook: 'onEnter', duration: 1200, tweenChanges: true,})
                    .reverse(false)
                    .addTo(controller)
                    .setTween(tween);

                    var tween2 = TweenMax.staggerFromTo('.tree .item',0.4,{opacity:0},{opacity:1, force3D: !0, ease: Power2.easeOut},0.1);
                    new ScrollMagic.Scene({triggerElement: "#about .tree .three", triggerHook: 'onEnter', duration: 600, tweenChanges: true,})
                        .reverse(false)
                        .setTween(tween2)
                        .addTo(smController);
                }

                if($('#name').length > 0)
                {

                    var tween2 = TweenMax.staggerFromTo('#name .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#name .big-text-box',triggerHook: "onEnter",offset:$('#name .big-text-box').height()/1.5})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#name',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#name .big-text-box',{y:'-80%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#name .child',2,{opacity:0,y:250},{opacity:1,y:0,delay:1.2, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#name .big-text-box',triggerHook: "onEnter",offset:$('#name .big-text-box').height()/1.15})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

                // Strip 1
                if($('#member1').length > 0)
                {
                    new ScrollMagic.Scene({triggerElement: '#member1',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#member1 video',{y:'50%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var orig = document.querySelector('#member1 .circle');
                    var obj = {length:0, pathLength:400};

                    var tween =  new TimelineMax().add([
                        TweenLite.to("#member1 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member1 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member1 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj, 1.5, {length:obj.pathLength, onUpdate:function(){orig.style.strokeDasharray = [obj.length,obj.pathLength].join(' ');}, ease:Linear.easeNone})
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member1',triggerHook: "onEnter",offset:$('#member1').height()/1.5})
                        .setTween(tween)
                        .reverse(false)
                        .addTo(smController);

                }

                // Strip 2
                if($('#member2').length > 0)
                {
                    var orig2 = document.querySelector('#member2 .circle');
                    var obj2 = {length:0, pathLength:400};

                    var tween2 =  new TimelineMax().add([
                        TweenLite.to("#member2 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member2 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member2 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj2, 1.5, {length:obj2.pathLength, onUpdate:function(){orig2.style.strokeDasharray = [obj2.length,obj2.pathLength].join(' ');}, ease:Linear.easeNone}),
                        TweenMax.staggerFromTo('#member2 img',1.8,{opacity:0,x:-400},{opacity:1,x:0,delay:0.3, force3D: !0, ease: Power2.easeOut},0.5)
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member2',triggerHook: "onEnter",offset:$('#member2').height()/1.5})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                }

                // Strip 3
                if($('#member3').length > 0)
                {
                    new ScrollMagic.Scene({triggerElement: '#member3',duration:"200%",triggerHook: "onEnter"}) 
                        .setTween('#member3 video',{y:'50%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var orig3 = document.querySelector('#member3 .circle');
                    var obj3 = {length:0, pathLength:400};

                    var tween =  new TimelineMax().add([
                        TweenLite.to("#member3 .profile_image", 1, {className:"+=active"}),
                        TweenMax.staggerFromTo('#member3 .animate',0.8,{opacity:0,y:50},{opacity:1,y:0, force3D: !0, ease: Power1.easeOut,delay:0.3},0.25),
                        TweenMax.fromTo('#member3 .profile_image span',1.3,{opacity:0},{opacity:1, force3D: !0, ease: Power1.easeOut}),
                        TweenMax.to(obj3, 1.5, {length:obj3.pathLength, onUpdate:function(){orig3.style.strokeDasharray = [obj3.length,obj3.pathLength].join(' ');}, ease:Linear.easeNone}),
                        TweenMax.staggerFromTo('#member3 img',2.8,{opacity:0,y:400},{opacity:1,y:0,delay:0.3, force3D: !0, ease: Power2.easeOut},0.5)
                    ]);

                    new ScrollMagic.Scene({triggerElement: '#member3',triggerHook: "onEnter",offset:$('#member3').height()/1.5})
                        .setTween(tween)
                        .reverse(false)
                        .addTo(smController);

                }

                // Section 4
                if($('#gun').length > 0)
                {

                    var tween2 = TweenMax.fromTo('#gun .title-cloud',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#gun',triggerHook: "onEnter",offset:$('#gun').height()/3.0})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#gun .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#gun',triggerHook: "onEnter",offset:$('#gun').height()/3.0})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#gun',duration:"200%",offset:$('#gun .inner-page').height()/1.2,triggerHook: "onEnter"})
                        .setTween('#gun .big-text-box',{y:'100%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);


                      var tween2 = TweenMax.staggerFromTo('#gun .image_big .img',2,{x:1000},{x:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#gun',triggerHook: "onEnter",offset:$('#gun').height()/2})
                        .setTween(tween2) 
                        .addTo(smController);
                }

                // Section 5
                if($('#death').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#death .title-cloud',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#death .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#death .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#death .big-text-box',triggerHook: "onEnter",offset:$('#death .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#death .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#death .big-text-box',{y:'-30%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#death img.image_big ',3.3,{opacity:0,y:150},{opacity:1,y:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#death .big-text-box',triggerHook: "onEnter",offset:$('#death .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

                // Section 6
                if($('#help').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#help .title-cloud',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#help .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#help .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#help .big-text-box',triggerHook: "onEnter",offset:$('#help .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#help .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#help .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#help img.image_big ',2.3,{opacity:0,y:150},{opacity:1,y:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#help .big-text-box',triggerHook: "onEnter",offset:$('#help .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }



                 // Section 7
                if($('#young').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#young .title-cloud',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#young .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#young .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#young .big-text-box',triggerHook: "onEnter",offset:$('#young .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#young .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#young .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController); 

                   var tween2 = TweenMax.staggerFromTo('#young img.image_big ',2.3,{opacity:0,y:150},{opacity:1,y:0,delay:0.5, force3D: !0, ease: Power2.easeOut},0.5);
                    new ScrollMagic.Scene({triggerElement: '#young .big-text-box',triggerHook: "onEnter",offset:$('#young .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }


                  // Section 8
                if($('#new_home').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#new_home .title-cloud',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#new_home .title-cloud',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    var tween2 = TweenMax.staggerFromTo('#new_home .big-text-box .animate',1,{opacity:0,y:60},{opacity:1,y:0, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#new_home .big-text-box',triggerHook: "onEnter",offset:$('#new_home .big-text-box').height()/1.55})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);

                    new ScrollMagic.Scene({triggerElement: '#new_home .inner-page',duration:"200%",triggerHook: "onEnter"})
                        .setTween('#new_home .big-text-box',{y:'-70%', force3D: !0, ease: Linear.easeNone})
                        .addTo(smController); 
                }

                 // Section 9
                if($('#life').length > 0)
                {
                    var tween2 = TweenMax.fromTo('#life .title-cloud-dark',1,{opacity:0},{opacity:1, force3D: !0, ease: Linear.ease},0.15);
                    new ScrollMagic.Scene({triggerElement: '#life .title-cloud-dark',offset:'50%'})
                        .setTween(tween2)
                        .reverse(false)
                        .addTo(smController);
                }

            }

        }
        
    // Scrollbars
        function scrollbars()
        {
            if(desktopRes <= getViewport_width())
            {
                if(!$('body').hasClass('scrollbars'))
                {
                    $('body').addClass('scrollbars');
                    $('.cscrollbar').mCustomScrollbar({scrollInertia:250,theme:'minimal-dark',mouseWheel:{ preventDefault: true },keyboard:{enable:true,scrollType:"stepless",scrollAmount:"auto"}});
                }
            }
            else
            {
                if($('body').hasClass('scrollbars'))
                {
                    $('body').removeClass('scrollbars');
                    $(".customscrollbar-dark").mCustomScrollbar("destroy").css('overflow','');
                }
            }
        }

    // arrow and scroll to top
        var mywindow = $(window);
        var mypos = mywindow.scrollTop();
        var up = false;
        var newscroll;
        
        mywindow.scroll(function () {
            newscroll = mywindow.scrollTop();
            if (newscroll > mypos && !up) {
                $('.center').addClass('arrow-up');
                up = !up;
            } else if(newscroll < mypos && up) {
                $('.center').removeClass('arrow-up');
                up = !up;
            }
            mypos = newscroll;
    });



      $('.backtotop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });


        // Form submit
          function setFormMethods() {
            $('#contact_form').submit(function (event) {
                var action = 'post_contact_form';
                var form = jQuery(this);
                var first = form.find('#first').val();
                var last = form.find('#last').val();
                var email = form.find('#email').val();
                var destination = form.find('#destination').val();
                var message = form.find('#message').val();

                jQuery.ajax({
                    url: '/form.php',
                    data: 'action=' + action + '&first=' + first + '&last=' + last + '&email=' + email + '&destination=' + destination + '&message=' + message,
                    type: 'post',
                    dataType: 'json',
                    success: function (result) {
                        if (result.status == 'error') {

                            jQuery('#form_errors li').remove();
                            jQuery.each(result.errors, function (index, value) {
                                jQuery('#form_errors ul').append('<li>' + value + '</li>');
                            });
                            jQuery('#form_errors').slideDown('slow', function () {
                                jQuery('.cscrollbar').mCustomScrollbar("scrollTo", '#form_errors');
                            });
                        }
                        else if (result.status == 'success') {
                            jQuery('#form_errors').hide();
                            jQuery('#contact_form').fadeOut('slow', function () {
                                jQuery('#form_success').fadeIn('slow');
                            });

                        }
                    }
                });
                event.preventDefault();

            });
        }
